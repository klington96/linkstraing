﻿
#include<algorithm>
#include <iostream>
#include <string>
#include <functional>

class Pdata 
{
public:


	Pdata() : pname(" "), score(0)
	{


	}

	Pdata(std::string _pname, int _score) : pname(_pname), score(_score)
	{
	
	
	}

	int GetScore() 
	{
		return score;

	}

	void Print() 
	{
		std::cout << pname << " " << score << "\n";

	}


	//void insertionsort(int* l, int* r) 
	//{

	//	for (int* i = l + 1; i < r; i++) {
	//		int* j = i;
	//		while (j > l && *(j - 1) > *j) {
	//			std::swap(*(j - 1), *j);
	//			j--;
	//		}
	//	}
	//}




private:

	std::string pname;
	int score;



};

bool comparator(Pdata& first, Pdata& second)
{
	return first.GetScore() > second.GetScore();
}

int main()
{
	std::cout << "Enter an integer: ";
	int p_count;
	std::cin >> p_count;
	Pdata *p_array = new Pdata[p_count];
	

	for (int i = 0; i < p_count; i++)
	{
		std::string p_name;
		int p_score;
		std::cout << "Enter player name: ";
		std::cin >> p_name;
		std::cout << "Enter player score: ";
		std::cin >> p_score;
		p_array[i] = Pdata (p_name, p_score);


	}
	

	std::sort(p_array, p_array + p_count, comparator);

	for (int i = 0; i < p_count; i++) 
	{
		p_array[i].Print();

	}
	
	delete[] p_array;
	
	return 0;


}

